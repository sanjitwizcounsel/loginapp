import * as types from '../constants/auth'
import localStorage from '../utils/localStorage'
import {apiv1} from "../api";
import {USER} from "../constants/auth";
import {PROFESSIONAL} from "../constants/auth";
// import * as type from './../actions/types'

function getProfile(data) {
    const profile = {};
    profile.first_name = data.first_name;
    profile.last_name = data.last_name;
    profile.email = data.email;
    profile.username = data.username;
    profile.email_verified = data.email_verified;
    return profile
}

const initialState = {
    client: {},
    professional: {},
    basicDetails: {},
    is_super_user: false,
    is_staff_user: false
};

export default function session(state = initialState, action) {
    var out = {
        ...state
    };
    switch (action.type) {
        case types.AUTH_TOKEN:
        case types.SIGNUP:
            // debugger
            if (!localStorage.selectedProfessionalDetails) {
                // localStorage.clear()
            }
            localStorage.authToken = 'Bearer ' + action.payload.auth_token;
            if (action.type === types.SIGNUP){
                localStorage.userType = action.payload.user_type;
                localStorage.firstName = action.payload.first_name;
                localStorage.lastName = action.payload.last_name
            }
            out.auth_token = action.payload.auth_token;
            // console.log(localStorage)
            return out;
        case types.LOGOUT:
            localStorage.clear();
            out.professional = {};
            out.client = {};
            out.basicDetails = {};
            // console.log(localStorage)
            apiv1.setUthTokenToNull(null);
            return out;
        case types.DETAILS:
            out.client = action.payload.client;
            if (state.is_super_user && action.payload.professional && action.payload.client.is_default_professional){
                out.client = {...state.client}
            }
            localStorage.firstName = action.payload.first_name;
            localStorage.lastName = action.payload.last_name;
            if (localStorage.userType === USER){
                localStorage.clientUserId = action.payload.client.user
            }
            if (localStorage.userType === PROFESSIONAL){
                out.professional_user_id = action.payload.id
                localStorage.professionalId = action.payload.professional.id
            }
            out.professional = action.payload.professional;
            out.basicDetails = getProfile(action.payload);
            return out;
        case types.UPLOAD_PROFILEPIC:
            out.professional.avatar = action.payload;
            return out;
        case types.UPDATE_USER_PHONE:
            if (!out.client.phone){
                out.client.phone = {}
            }
            out.client.phone = action.payload;
            return out;
        case types.UPDATE_PROFESSIONAL_PHONE:
            if (!out.professional.phone){
                out.professional.phone = {}
            }
            out.professional.phone = action.payload;
            return out;
        case types.VERIFY_PHONE:
            out.professional.phone.verified = action.payload.success;
            return out;
        case types.VERIFY_EMAIL:
            out.basicDetails.email_verified = action.payload;
            return out;
        case types.UPDATE_PROFESSIONAL_BANK_DETAILS:
            out.professional.bank_account = action.payload;
            return out;
        case types.SUPER_USER:
            out.is_super_user = action.payload;
            return out;
        case types.CLAIM_REFERRED_MONEY:
            out.client.referral_credits_earned = action.payload;
            return out;
        default:
            return state
    }
}

