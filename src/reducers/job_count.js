import * as types from "../constants/auth";
const user = {
    jobInProgressCount: 0,
    jobPostingCount: 0,
    consultationCount: 0,
    jobCompletedCount: 0,
    consultationCompletedCount: 0,
};
const professional = {
    jobInProgressCount: 0,
    jobProposalSentCount: 0,
    jobCompletedCount : 0,
    consultationCount: 0,
    jobInviteCount: 0,
    consultationCompletedCount: 0,
};
const initialState = {
    user,
    professional
};

export default function jobCount (state = initialState, action) {
    var out = {
        ...state
    };
    let user = {
        ...state.user
    };
    let professional = {
        ...state.professional
    };
    switch (action.type) {
        case types.USER_JOB_POSTING_COUNT:
            user.jobPostingCount = action.payload;
            out.user = user;
            return out;
        case types.USER_JOB_IN_PROGRESS_COUNT:
            user.jobInProgressCount = action.payload;
            out.user = user;
            return out;
        case types.PROFESSIONAL_JOB_IN_PROGRESS_COUNT:
            professional.jobInProgressCount = action.payload;
            out.professional = professional;
            return out;
        case types.PROFESSIONAL_PROPOSAL_SENT_COUNT:
            professional.jobProposalSentCount = action.payload;
            out.professional = professional;
            return out;
        case types.PROFESSIONAL_SIDEBAR_JOB_COUNT:
            professional.jobInProgressCount = action.payload.job_in_progress;
            professional.jobProposalSentCount = action.payload.proposal_count;
            professional.jobCompletedCount = action.payload.job_completed;
            professional.consultationCount = action.payload.consult;
            professional.jobInviteCount = action.payload.invite_count;
            professional.consultationCompletedCount = action.payload.consult_completed
            out.professional = professional;
            return out;
        case types.PROFESSIONAL_JOB_INVITES_COUNT:
            professional.jobInviteCount = action.payload
            out.professional = professional;
            return out;
        case types.USER_SIDEBAR_JOB_COUNT:
            user.jobInProgressCount = action.payload.job_in_progress;
            user.jobPostingCount = action.payload.job_post;
            user.consultationCount = action.payload.consult;
            user.jobCompletedCount = action.payload.job_completed;
            user.consultationCompletedCount = action.payload.consult_completed
            out.user = user;
            return out;
        default:
            return state
    }
}