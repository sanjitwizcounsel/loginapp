import * as types from "../constants/auth";
const initialAmount = {
    job_in_progress_amount: 0,
    user_invoice_total_amount: 0,
    user_invoice_total_amount_tax : 0,
    wizcounsel_invoice_total_amount: 0,
    wizcounsel_invoice_total_amount_tax: 0,
};

export default function accounting (state = initialAmount, action) {
    var out = {
        ...state
    };
    const {type, payload} = action
    switch (type) {
        case types.ACCOUNTING:
            const {job_in_progress_amount, user_invoice_total_amount, user_invoice_total_amount_tax, wizcounsel_invoice_total_amount, wizcounsel_invoice_total_amount_tax} = payload
            return {
                ...out,
                job_in_progress_amount,
                user_invoice_total_amount,
                user_invoice_total_amount_tax,
                wizcounsel_invoice_total_amount,
                wizcounsel_invoice_total_amount_tax
            }

        default:
            return state
    }
}