export const PUSH_ERROR = 'error.push';
export const POP_ERROR = 'error.pop';
export const CLEAR_ERRORS = 'error.clear';
export const CLEAR_AND_SHOW_ERROR = 'error.clear_n_show';
export const CLEAR_AND_SHOW_POPUP = 'error.clear_n_show_popup';
