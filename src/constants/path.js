export const userLogin = '/login';
export const userRegistration = '/user-registration';
export const professionalLogin = '/login';
export const professionalRegistration = '/professional-registration';
export const userOnboarding = '/user-onboarding';
export const generalOnboarding = '/hiring-summary';
export const professionalDashboard = '/professional-dashboard';
export const professionalProfile = '/professional-profile';
export const jobFeed = '/job-feed';
export const documents = '/documents';
export const statusTrack = '/status-track';
export const jobHistory = '/job-history';
export const consultHistory = '/consult-history';
export const notifications = '/notifications';
export const chats = '/chats';
export const proposalSent = '/proposal-sent';
export const proposalSentLive = '/live-proposals';
export const proposalSentNotLive = '/proposals-not-live';
export const professionalJobInProgress = '/professional/job-in-progress';
export const userJobInProgress = '/user/job-in-progress';
export const userDashboard = '/user-dashboard';
export const root = '/';
export const home = '/';
export const userProfile = '/user-profile';
// export const jobInProgress = '/job-in-progress'
export const jobPosting = '/job-posting';
export const userDocuments = '/user-documents';
export const postedJobs = '/user/posted-jobs';
export const postedLiveJobs = '/user/posted-live-jobs';
export const postedNotLive = '/user/posted-not-live';
export const track = '/track';
export const usersProfile = '/user-profile';
export const userHistory = '/user-history';
export const postAJob = '/job-post';
export const editJob = '/edit-job';
export const manuallyJobCreate = '/manually-job-create';
export const assignJobToProfessional = '/assign-job-to-professional';
export const addChequePayment = '/cheque-payment';
export const professionalList = '/professional-list';
export const adminProfessionalList = '/admin-professional-list';
export const invite = 'invite';
export const perHour = 'per-hour';
export const halfAnHour = 'half-an-hour';
export const forConsultation = 'for-consultation';
export const inviteForConsultationPerHour = '/invite-for-consultation/per-hour';
export const inviteForConsultationHalfAnHour = '/invite-for-consultation/half-an-hour';
export const findAProfessionalByLocation = '/we-will-find-for-you';

// export const userDocuments = '/user-documents'

//Professional paths
export const resetPasswordLink = '/reset-password-link';
export const resetPassword = '/password/reset';
export const eamilConfirmed = '/registration/account-confirm-email';
export const thankYou = '/thank-you';
export const professionalLanding = '/for-professionals';
export const feedbackForConsultation = 'feedback';

//user
export const userConsultationList = '/consultations-list';
export const documentCheckList = '/check-list';
export const invoice = '/invoice-for';
export const job='job'
export const list='list'

// chat
export const chatToUser = '/chat-to-user';
export const chatToProfessional = '/chat-to-professional';
export const userChatHistory = 'user-chat-history';
export const professionalChatHistory = '/professional-chat-history';
export const practiceArea = '/your-practice-area';

//Learning center Question with answer
export const homeLearningCenter = '/income-tax-india-kanoon';
export const Learningcenter = '/income-tax-india-kanoon';
export const EnterpriseServices = '/for-startups';
export const testimonials = '/our-testimonials';
//About site
export const termsAndConditions = '/terms-of-service';
export const privacyPolicy = '/privacy-policy';
export const getInTouch = '/about-us';
export const Howitwork = '/user-how-it-work';
export const ProfessionalHowitwork = '/professional-how-it-work';

export const assignslots = '/assign-slot';
export const selectslot = '/select-time-slot';

//Business formation services
export const taxAdd = '/file-income-tax-online';
export const sendReplyLegalNotice = '/find-lawyers';
export const courtRepresentation = '/hire-lawyers';
export const businessFormationSub = '/company-registration-online';
export const businessFormationLlpSub = '/llp-registration-online';
export const businessFormationPartnershipSub = '/partnership-firm-registration-online';
export const soleproPrietorshipSub = '/sole-proprietorship-registration-online';
export const onePersonCompanySub = '/one-person-company-registration-online';
export const sectionEightecompany = '/ngo-registration-online';
export const companyStrikeOff = '/company-name-strike-off';
export const incomeTaxSalaried = '/income-tax-filing-online-salaried';
export const incomeTaxCapitalGain = '/itr-filing-online';
export const incomeTaxFilingSmallBusinesses = '/income-tax-filing-online-small-business';
export const accountingServices500 = '/accountants-online/accountants-small-business';
export const accountingServices1500 = '/bookkeepers-online/bookkeeping-small-business';
export const accountingServices2500 = '/find-an-accountant/accountants-near-me';
export const taxConsultation = '/tax-consultation-online';
export const gstReturnFiling = '/gst-return-filing-online';
export const gstRegistration = '/gst-registration-online';
export const annualRocFiling = '/roc-filing-online';
export const annualRocCompliance = '/roc-compliance-online';
export const trademarkRegistrationForCompany = '/trademark-registration-online-companies';
export const trademarkRegistrationForIndividual = '/trademark-registration-online-freelancers';
export const licensingAgreement = '/licensing-agreement-drafting';
export const webAgreement = '/terms-of-use-privacy-policy-drafting';
export const franchiseeAgreement = '/franchisee-agreement-drafting';
export const intellectualPropertyAgreement = '/trademark-agreement-drafting';
export const saleDeedAgreement = '/sales-deed-drafting';
export const vendorContractAgreement = '/vendor-contract-agreement-drafting';
export const shareholdingAgreement = '/shareholding-agreement-drafting-startup-lawyers';
export const termSheetEquity = '/term-sheet-drafting-startup-lawyers';
export const founderAgreement = '/founders-agreement-drafting-startup-lawyers';
export const debtFinancingLoanAgreement = '/loan-agreement-drafting';
export const employeeStockPptionPolicy = '/esop-policy-drafting-startup-lawyers';
export const employmentAgreement = '/employment-agreement-drafting';
export const rentAgreement = '/rent-agreement-drafting-online';
export const ndaAgreement = '/non-disclosure-agreement-drafting';
export const product = 'product';
export const practiceAreaId = 'practicearea';
export const productFilter = 'product-filter';
export const legal = 'legal';
export const city = 'city';
export const professionalByProfession = 'professional-by';
export const profession = 'profession';
export const requestFreeProposal = 'request-free-proposal';

export const consultationFilter = '/consultation-filter';
export const consultationProfessionalList = '/legal-advice-online/lawyers-near-me';
export const consultationDetailsPage = '/consultation-details-new';
export const consultationOnboarding = '/consultation-onboarding';
export const askLegalQuestion = '/ask-legal-question';
export const askTaxQuestion = '/ask-tax-question';
export const chatToProfessionalAfterHire = 'chat-to-professional-after-hire';
export const jobDetails = 'job-details';
export const findAlawyer = '/find-a-lawyer/lawsuits-and-disputes';
export const findLawyerOnline = '/find-lawyers-online';
export const findCaOnline = '/chartered-accountants-near-me';
export const findBookKeeperOnline = '/find-bookkeepers-online';
export const findCsOnline = '/find-company-secretaries-online';

//Add
export const fileItr = '/ca-for-tax-filing-online'
export const accountingOnline = '/hire-accountants-bookkeepers-online'
export const accountingDetails = '/accounting-details'
export const adminMilestoneMap = '/milestone-map'
export const addQuestion = '/add-questions'
export const sendProposal = '/send-proposal'


