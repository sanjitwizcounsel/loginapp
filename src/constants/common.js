const exotelCallerId = "01140849277"
const exotelAuthToken = "Basic 27cb0140f69f18fdfa7765caa5e93954c739241dd83120f2"
const apiKey = '4ac5330b9d49ef1f4fa5f531cd1a9d94cfc6351593110ae1'
const exotelSid = 'wizcounsel'
const requestJobCompletionRequestMessage = 'Your job completion request has been sent, please wait till the user approves the request'




export {
    exotelCallerId,
    exotelAuthToken,
    apiKey,
    exotelSid,
    requestJobCompletionRequestMessage
}