import * as path from "./path";
import MetaTags from "react-meta-tags";
import React from "react";

export const status = {
    Posted: 'Posted',
    Paid: 'Paid',
    Completed: 'Completed',
    Closed: 'Closed',
    InProgress: 'In Progess',
    InDispute: 'In Dispute',
    Migrated: 'Migrated'
}

export const professionalStatus = {
    busy: 'busy',
    active: 'active'
}


export const profession = {
    1: {
        text: 'Chartered Accountants',
        professional_name: 'caList',
        path: path.findCaOnline,
        profession_type: '/find-chartered-accountants',
        meta_tag:[
            <MetaTags>
                <title>Find Chartered Accountants in India for Tax & Accounting Services | WizCounsel</title>
                <meta name="title"
                      content="Find Chartered Accountants in India for Tax & Accounting Services | WizCounsel"/>
                <meta name="description" content="Find & hire freelance Chartered Accountants online for Tax & Accounting Services at affordable prices. Get Payment Insurance and dedicatedsupport manager."/>
                <meta name="keywords"
                      content="Chartered Accountant near me, CA near me, freelance accountant jobs, freelance accounting work, Civil Lawyers near me, Corporate Lawyer near me, Tax Attorney near me, Tax Lawyers near me, Online Lawyers India, Online legal work from home jobs, Startup Lawyers, Lawyers near me"/>
                <meta name="url" content="https://wizcounsel.com/chartered-accountants-near-me"/>
                <meta property="og:url" content="https://wizcounsel.com/chartered-accountants-near-me"/>
                <meta property="og:type" content="website"/>
                <meta property="og:site_name" content="WizCounsel"/>
                <meta property="og:title"
                      content="Find Chartered Accountants in India for Tax & Accounting Services | WizCounsel"/>
                <meta property="og:description"
                      content="Find & hire freelance Chartered Accountants online for Tax & Accounting Services at affordable prices. Get Payment Insurance and dedicatedsupport manager."/>
                <meta property="og:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <meta property="og:image:width" content="300"/>
                <meta property="og:image:height" content="300"/>
                <meta name="twitter:card"
                      content="Find & hire freelance Chartered Accountants online for Tax & Accounting Services at affordable prices. Get Payment Insurance and dedicatedsupport manager."/>
                <meta name="twitter:site" content="WizCounsel"/>
                <meta name="twitter:title"
                      content="Find Chartered Accountants in India for Tax & Accounting Services | WizCounsel"/>
                <meta name="twitter:description"
                      content="Find & hire freelance Chartered Accountants online for Tax & Accounting Services at affordable prices. Get Payment Insurance and dedicatedsupport manager."/>
                <meta name="twitter:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <link rel="canonical" href="https://wizcounsel.com/chartered-accountants-near-me"/>
            </MetaTags>
        ]
    },
    2: {
        text: 'Lawyers',
        professional_name: 'lawyerList',
        path: path.findLawyerOnline,
        profession_type: '/find-lawyers',
        meta_tag:[
            <MetaTags>
                <title>Find Lawyers in India for Legal Services | WizCounsel</title>
                <meta name="title"
                      content="Find Lawyers in India for Legal Services | WizCounsel"/>
                <meta name="description" content="Find & hire Lawyers online for various Legal Services at affordable prices. Get Payment Insurance and dedicated legal manager."/>
                <meta name="keywords"
                      content="Chartered Accountant near me, CA near me, freelance accountant jobs, freelance accounting work, Civil Lawyers near me, Corporate Lawyer near me, Tax Attorney near me, Tax Lawyers near me, Online Lawyers India, Online legal work from home jobs, Startup Lawyers, Lawyers near me"/>
                <meta name="url" content="https://wizcounsel.com/find-lawyers-online"/>
                <meta property="og:url" content="https://wizcounsel.com/find-lawyers-online"/>
                <meta property="og:type" content="website"/>
                <meta property="og:site_name" content="WizCounsel"/>
                <meta property="og:title"
                      content="Find Lawyers in India for Legal Services | WizCounsel"/>
                <meta property="og:description"
                      content="Find & hire Lawyers online for various Legal Services at affordable prices. Get Payment Insurance and dedicated legal manager."/>
                <meta property="og:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <meta property="og:image:width" content="300"/>
                <meta property="og:image:height" content="300"/>
                <meta name="twitter:card"
                      content="Find & hire Lawyers online for various Legal Services at affordable prices. Get Payment Insurance and dedicated legal manager."/>
                <meta name="twitter:site" content="WizCounsel"/>
                <meta name="twitter:title"
                      content="Find Lawyers in India for Legal Services | WizCounsel"/>
                <meta name="twitter:description"
                      content="Find & hire Lawyers online for various Legal Services at affordable prices. Get Payment Insurance and dedicated legal manager."/>
                <meta name="twitter:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <link rel="canonical" href="https://wizcounsel.com/find-lawyers-online"/>
            </MetaTags>
        ]
    },
    3: {
        text: 'Accountants & Bookkeepers',
        professional_name: 'bookKeepingList',
        path: path.findBookKeeperOnline,
        profession_type: '/find-bookkeepers',
        meta_tag:[
            <MetaTags>
                <title>Hire freelance Accoutants & Bookeepers Online | WizCounsel</title>
                <meta name="title"
                      content="Hire freelance Accoutants & Bookeepers Online | WizCounsel"/>
                <meta name="description" content="Find & hire freelance Accoutants & Bookeepers Online for Accounting & Bookkeeping services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="keywords"
                      content="Chartered Accountant near me, CA near me, freelance accountant jobs, freelance accounting work, Civil Lawyers near me, Corporate Lawyer near me, Tax Attorney near me, Tax Lawyers near me, Online Lawyers India, Online legal work from home jobs, Startup Lawyers, Lawyers near me"/>
                <meta name="url" content="https://wizcounsel.com/find-bookkeepers-online"/>
                <meta property="og:url" content="https://wizcounsel.com/find-bookkeepers-online"/>
                <meta property="og:type" content="website"/>
                <meta property="og:site_name" content="WizCounsel"/>
                <meta property="og:title"
                      content="Hire freelance Accoutants & Bookeepers Online | WizCounsel"/>
                <meta property="og:description"
                      content="Find & hire freelance Accoutants & Bookeepers Online for Accounting & Bookkeeping services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta property="og:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <meta property="og:image:width" content="200"/>
                <meta property="og:image:height" content="200"/>
                <meta name="twitter:card"
                      content="Find & hire freelance Accoutants & Bookeepers Online for Accounting & Bookkeeping services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="twitter:site" content="WizCounsel"/>
                <meta name="twitter:title"
                      content="Hire freelance Accoutants & Bookeepers Online | WizCounsel"/>
                <meta name="twitter:description"
                      content="Find & hire freelance Accoutants & Bookeepers Online for Accounting & Bookkeeping services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="twitter:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <link rel="canonical" href="https://wizcounsel.com/find-bookkeepers-online"/>
            </MetaTags>
        ]
    },
    5: {
        text: 'Company Secretaries',
        professional_name: 'secretarialList',
        path: path.findCsOnline,
        profession_type: '/find-bookkeepers',
        meta_tag:[
            <MetaTags>
                <title>Find Company Secretaries in India for Compliance services| WizCounsel</title>
                <meta name="title"
                      content="Find Company Secretaries in India for Compliance services| WizCounsel"/>
                <meta name="description" content="Looking for a Company Secretary? Find & hire Company Secretaries online for various Compliance Services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="keywords"
                      content="Chartered Accountant near me, CA near me, freelance accountant jobs, freelance accounting work, Civil Lawyers near me, Corporate Lawyer near me, Tax Attorney near me, Tax Lawyers near me, Online Lawyers India, Online legal work from home jobs, Startup Lawyers, Lawyers near me"/>
                <meta name="url" content="https://wizcounsel.com/find-company-secretaries-online"/>
                <meta property="og:url" content="https://wizcounsel.com/find-company-secretaries-online"/>
                <meta property="og:type" content="website"/>
                <meta property="og:site_name" content="WizCounsel"/>
                <meta property="og:title"
                      content="Find Company Secretaries in India for Compliance services| WizCounsel"/>
                <meta property="og:description"
                      content="Looking for a Company Secretary? Find & hire Company Secretaries online for various Compliance Services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta property="og:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <meta property="og:image:width" content="300"/>
                <meta property="og:image:height" content="300"/>
                <meta name="twitter:card"
                      content="Looking for a Company Secretary? Find & hire Company Secretaries online for various Compliance Services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="twitter:site" content="WizCounsel"/>
                <meta name="twitter:title"
                      content="Find Company Secretaries in India for Compliance services| WizCounsel"/>
                <meta name="twitter:description"
                      content="Looking for a Company Secretary? Find & hire Company Secretaries online for various Compliance Services at affordable prices. Get Payment Insurance and dedicated support manager."/>
                <meta name="twitter:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                <link rel="canonical" href="https://wizcounsel.com/find-company-secretaries-online"/>
            </MetaTags>
        ]
    },
}