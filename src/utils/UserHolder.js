
export class UserHolder {
  set data (payload) {
    this._data = payload
  }

  get data () {
    return this._data
  }

  isPresent () {
    return Boolean(this._data)
  }

  get firstName () {
    if (this._data.name) {
      return this._data.name.split(' ')[0]
    } else if (this._data.email) {
      return this._data.email.split('@')[0]
    } else if (this._data.phone) {
      return this._data.phone
    } else {
      return ''
    }
  }
  get name () {
    return this._data.name || 'N/A'
  }

  get email () {
    return this._data.email || 'N/A'
  }

  get phone () {
    return this._data.phone || 'N/A'
  }
}

const instance = new UserHolder();

export default instance
