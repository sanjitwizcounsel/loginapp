const KEY_FCM_TOKEN = 'fcm_token';
const KEY_SELECTED_SERVICE_ID = 'selected_service_id';
const KEY_AUTH_TOKEN = 'auth_token';
export const KEY_USER_TYPE = 'user_type';
const KEY_FIRST_NAME = 'first_name';
const KEY_LAST_NAME = 'last_name';
const KEY_USER_JOB_POSTING_COUNT = 'user_job_posting_count';
const KEY_PROFESSIONAL_ID = 'professional_id';
const KEY_PROFESSIONAL_DETAILS = 'professional_details';
const KEY_USER_ID = 'user_id';
export const BOOK_CONSULTATION = 'book_consultation';
const KEY_CONSULTATION_DETAILS = 'consultation_details';
export const KEY_CITY_ID = 'city_id';
export const KEY_PROFESSION_ID = 'profession_id';
export const KEY_IS_VERIFIED_PROFESSIONAL = 'professional_verified';
export const KEY_ACTIVE_PAGE_NUMBER = 'active_page';
export const KEY_REFERRAL_CODE = 'referral_code';
export const KEY_PATH_NAME = 'path_name';

class LocalStorage {

    set fcmToken(value) {
        window.localStorage.setItem(KEY_FCM_TOKEN, value)
    }
    get fcmToken() {
        return window.localStorage.getItem(KEY_FCM_TOKEN)
    }
    set selectedServiceId(value) {
        window.localStorage.setItem(KEY_SELECTED_SERVICE_ID, value)
    }
    get selectedServiceId() {
        return window.localStorage.getItem(KEY_SELECTED_SERVICE_ID)
    }

    get authToken() {
        return window.localStorage.getItem(KEY_AUTH_TOKEN)
    }

    set authToken(value) {
        window.localStorage.setItem(KEY_AUTH_TOKEN, value)
    }
    get userType() {
        return window.localStorage.getItem(KEY_USER_TYPE)
    }

    set userType(value) {
        window.localStorage.setItem(KEY_USER_TYPE, value)
    }

    get firstName() {
        return window.localStorage.getItem(KEY_FIRST_NAME)
    }

    set firstName(value) {
        window.localStorage.setItem(KEY_FIRST_NAME, value)
    }

    get lastName() {
        return window.localStorage.getItem(KEY_LAST_NAME)
    }

    set lastName(value) {
        window.localStorage.setItem(KEY_LAST_NAME, value)
    }

    get userJobPostingCount() {
        return window.localStorage.getItem(KEY_USER_JOB_POSTING_COUNT)
    }

    set userJobPostingCount(value) {
        window.localStorage.setItem(KEY_USER_JOB_POSTING_COUNT, value)
    }
    get clientUserId() {
        return window.localStorage.getItem(KEY_USER_ID)
    }
    set clientUserId(value) {
        window.localStorage.setItem(KEY_USER_ID, value)
    }

    get professionalId() {
        return window.localStorage.getItem(KEY_PROFESSIONAL_ID)
    }

    set professionalId(value) {
        window.localStorage.setItem(KEY_PROFESSIONAL_ID, value)
    }
    get selectedProfessionalDetails() {
        return window.localStorage.getItem(KEY_PROFESSIONAL_DETAILS)
    }

    set selectedProfessionalDetails(value) {
        window.localStorage.setItem(KEY_PROFESSIONAL_DETAILS, value)
    }
    get isBookConsultation() {
        return window.localStorage.getItem(BOOK_CONSULTATION)
    }

    set isBookConsultation(value) {
        window.localStorage.setItem(BOOK_CONSULTATION, value)
    }

    get consultationDetails() {
        return window.localStorage.getItem(KEY_CONSULTATION_DETAILS)
    }

    set consultationDetails(value) {
        window.localStorage.setItem(KEY_CONSULTATION_DETAILS, value)
    }
    get cityId() {
        return window.localStorage.getItem(KEY_CITY_ID)
    }

    set cityId(value) {
        window.localStorage.setItem(KEY_CITY_ID, value)
    }
    get professionId() {
        return window.localStorage.getItem(KEY_PROFESSION_ID)
    }

    set professionId(value) {
        window.localStorage.setItem(KEY_PROFESSION_ID, value)
    }
    get isVerifiedProfessional() {
        return window.localStorage.getItem(KEY_IS_VERIFIED_PROFESSIONAL)
    }

    set isVerifiedProfessional(value) {
        window.localStorage.setItem(KEY_IS_VERIFIED_PROFESSIONAL, value)
    }
    get activePageNumber() {
        return window.localStorage.getItem(KEY_ACTIVE_PAGE_NUMBER)
    }

    set activePageNumber(value) {
        window.localStorage.setItem(KEY_ACTIVE_PAGE_NUMBER, value)
    }
    get referralCode() {
        return window.localStorage.getItem(KEY_REFERRAL_CODE)
    }

    set referralCode(value) {
        window.localStorage.setItem(KEY_REFERRAL_CODE, value)
    }
    get pathName() {
        return window.localStorage.getItem(KEY_PATH_NAME)
    }

    set pathName(value) {
        window.localStorage.setItem(KEY_PATH_NAME, value)
    }

    clear() {
        window.localStorage.clear()
    }

    removeKey(KEY) {
        window.localStorage.removeItem(KEY)
    }

    isLoggedIn() {
        return (
            Boolean(this.authToken)
        )
    }

    isSessionValid() {
        return this.isLoggedIn()

    }

}

const instance = new LocalStorage();

export default instance
