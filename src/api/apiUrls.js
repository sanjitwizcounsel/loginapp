import {exotelCallerId} from './../constants/common'
const removeAnsOfPracticeAreaQuestions = `/job/job-answer`
const exhotelCall = `https://api.exotel.com/v1`
const transactionAmount = '/invoice/transection-value'
const restAuth = `rest-auth/user/`
const earnMoneyFromReferral = `users/refer`
const proposalFilterByTitle = `/job/proposal-filter/`
const createJobManually = `/job/create-job-manually`
const assignJobToProfessional = `/job/assign-job-to-professional`
const proposalListWithUserDetails = `/job/proposal-filter/`
const userExhotelCall = '/users/call'
const jobProposalList = '/job/proposal/'
const phoneNumberByUserId = '/users/phone/'
const productList = '/users/product/'
const getPractice = `users/practisearea/`
const shareThisJobWithOther = '/job/access-share'

export {
    removeAnsOfPracticeAreaQuestions,
    exhotelCall,
    transactionAmount,
    restAuth,
    earnMoneyFromReferral,
    proposalFilterByTitle,
    createJobManually,
    assignJobToProfessional,
    proposalListWithUserDetails,
    userExhotelCall,
    jobProposalList,
    phoneNumberByUserId,
    productList,
    getPractice,
    shareThisJobWithOther
}
