import axios from 'axios';
import localStorage from '../utils/localStorage';
import {clearAndShowError, pushError} from '../actions/errors';

export const env = {
    dev: 'dev',
    staging: 'staging',
    production: 'production',
    local_host: 'local_host'
};
export let apiBaseUrl = '';
export let siteBaseUrl = '';
if (process.env.REACT_APP_ENV === env.dev) {
    apiBaseUrl = process.env.REACT_APP_DEV_API_BASE_URL;
    siteBaseUrl = process.env.REACT_APP_DEV_SERVER_URL
}
else if (process.env.REACT_APP_ENV === env.staging) {
    apiBaseUrl = process.env.REACT_APP_STAGING_API_BASE_URL;
    siteBaseUrl = process.env.REACT_APP_STAGING_SERVER_URL
}
else if (process.env.REACT_APP_ENV === env.production) {
    apiBaseUrl = process.env.REACT_APP_PRODUCTION_API_BASE_URL;
    siteBaseUrl = process.env.REACT_APP_PRODUCTION_SERVER_URL
} else {
    apiBaseUrl = process.env.REACT_APP_DEV_API_BASE_URL;
    siteBaseUrl = process.env.REACT_APP_LOCAL_HOST_SERVER_URL
}

 siteBaseUrl = 'https://web-dev.wizcounsel.com/';
 // siteBaseUrl = 'https://wizcounsel.com/'

console.log('React app env', process.env);

// console.log(process.env.REACT_APP_ENV);
// console.log(process.env.REACT_APP_API_BASE_URL);

apiBaseUrl = 'https://api-dev.wizcounsel.com/'
 // apiBaseUrl = 'https://api.wizcounsel.com/'

class V1 {
    constructor() {
        this.actualUrl = apiBaseUrl;
        this.baseURL = `${this.actualUrl}`;
        this.authToken = null;
        this._noAuth = null;
        this._auth = null;
    }

    get url() {
        return this.actualUrl;
    }

    setUthTokenToNull(value) {
        this._auth = null
    }

    authHeaders() {
        this.authToken = localStorage.authToken;
        return {
            'Authorization': this.authToken,
            'x-wiz-device-type': 'WEB'
        };
    }

    get noAuth() {
        if (!this._noAuth) {
            this._noAuth = axios.create({ baseURL: this.baseURL });
        }
        return this._noAuth;
    }

    get auth() {
        this._auth = axios.create({
            baseURL: this.baseURL,
            headers: this.authHeaders(),
        });
        // console.log('existing auth',this._auth )
        return this._auth;
    }

    parse(axiosResponse) {
        try {
            const { data, status, statusText } = axiosResponse;
            switch (status) {
                case 201:
                case 200:
                case 204:
                    return {
                        data: data,
                        status: status,
                        error: null
                    };
                case 401:
                case 400:
                case 404:
                    return {
                        data: null,
                        status: status,
                        error: data
                    };
                default:
                    return {
                        data: null,
                        status: status,
                        error: statusText
                    };
            }
        } catch (e) {
            console.error(e);
            return {
                data: null,
                status: null,
                error: 'Oops, something went wrong, please try again'
            };
        }
    }

    parseAndAutoHandleIssues(axiosResponse, doDispatch) {
        let {
            data, error, status
        } = this.parse(axiosResponse);
        if (status === 401 || status === 400 || status === 404) {
            return error
        }
        if (error) {
            doDispatch(clearAndShowError(error));
            data = null;
        }
        return data;
    }

    handleErrors(axiosResponse, doDispatch) {
        if (axiosResponse.response !== undefined) {
            return this.parseAndAutoHandleIssues(axiosResponse.response, doDispatch)
        } else {
            const err = axiosResponse.message;
            doDispatch(clearAndShowError(err))
        }
        // }

    }
}

export const apiv1 = new V1();

class V2 {
    constructor(authToken) {
        this.actualUrl = apiBaseUrl;
        this.baseURL = `${this.actualUrl}`;
        this.authToken = authToken;
        this._noAuth = null;
        this._auth = null;
    }

    get url() {
        return this.actualUrl;
    }

    setUthTokenToNull(value) {
        this._auth = null
    }
    setAuthToken(authToken) {
        this.authToken = authToken;
    }

    authHeaders() {
        return {
            'Authorization': this.authToken,
            'x-wiz-device-type': 'WEB'
        };
    }

    get noAuth() {
        if (!this._noAuth) {
            this._noAuth = axios.create({ baseURL: this.baseURL });
        }
        return this._noAuth;
    }

    get auth() {
        this._auth = axios.create({
            baseURL: this.baseURL,
            headers: this.authHeaders(),
        });
        // console.log('existing auth',this._auth )
        return this._auth;
    }

    parse(axiosResponse) {
        try {
            const { data, status, statusText } = axiosResponse;
            switch (status) {
                case 201:
                case 200:
                case 204:
                    return {
                        data: data,
                        status: status,
                        error: null
                    };
                case 401:
                case 400:
                case 404:
                    return {
                        data: null,
                        status: status,
                        error: data
                    };
                default:
                    return {
                        data: null,
                        status: status,
                        error: statusText
                    };
            }
        } catch (e) {
            console.error(e);
            return {
                data: null,
                status: null,
                error: 'Oops, something went wrong, please try again'
            };
        }
    }

    parseAndAutoHandleIssues(axiosResponse, doDispatch) {
        let {
            data, error, status
        } = this.parse(axiosResponse);
        if (status === 401 || status === 400 || status === 404) {
            return error
        }
        if (error) {
            doDispatch(pushError(error));
            data = null;
        }
        return data;
    }

    handleErrors(axiosResponse, doDispatch) {
        if (axiosResponse.response !== undefined) {
            return this.parseAndAutoHandleIssues(axiosResponse.response, doDispatch)
        } else {
            const err = axiosResponse.message;
            doDispatch(pushError(err))
        }
        // }

    }
}
export const apiv2 = new V2();
