import * as types from "../constants/auth";

export const verifyEmail = (loginData) => {
    return {
        type: types.VERIFY_EMAIL,
        payload: loginData
    }
};

export const verifyPhone = (success) => {
    return {
        type: types.VERIFY_PHONE,
        payload: success
    }
};