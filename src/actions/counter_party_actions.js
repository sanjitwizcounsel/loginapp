import * as types from "../constants/auth";

export const storeUserData = (user_details) => {
    return {
        type: types.USER_DETAILS,
        payload: user_details
    }
};

export const storeProfessionalData = (professional_details) => {
    return {
        type: types.PROFESSIONAL_DETAILS,
        payload: professional_details
    }
};
