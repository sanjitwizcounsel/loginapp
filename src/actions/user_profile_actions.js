import * as types from './types'

export const updateProfilePic = (payload) => ({
    type: types.users.UPDATE_PROFILE_PIC,
    payload
});
//
// module.exports = {
//     updateProfilePic,
// }