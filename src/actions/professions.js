import * as types from "../constants/auth";

export const getProfessions = (professions) => {
    return {
        type: types.GET_PROFESSIONS,
        payload: professions
    }
};
