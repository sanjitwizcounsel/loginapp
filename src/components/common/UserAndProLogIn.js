import { Component } from "react";
import React from "react";
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import './../user/../../static/css/style.css'
import { errorActionDispatchers } from '../../actions/errors'
import {sessionLogin, userDetails} from '../../actions/auth'
import { apiv1 } from "../../api";
import LoadingComponent from "../../utils/LoadingComponent";
import axios from 'axios'
import {apiBaseUrl} from './../../api/index'
import * as path from "../../constants/path";
import MetaTags from "react-meta-tags";
import localStorage from "../../utils/localStorage";
import $ from "jquery";

class UserAndProLogIn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            loading: false,
            errors: {},
        }
    }
    passwordVisible = () =>{
        $("#password_visibility").click(function(){
            var pass_input = document.getElementById("password");
            if (pass_input.type === "password") {
                pass_input.type = "text";
                $(this).removeClass("fa-eye").addClass("fa-eye-slash")
            } else {
                pass_input.type = "password";
                $(this).removeClass("fa-eye-slash").addClass("fa-eye")
            }
        });
    }
    componentDidMount(){
        this.addEventListenerOnKeyDown()
        this.passwordVisible()
    }

    componentWillUnmount() {
        if (document.body.removeEventListener){
            document.body.removeEventListener('keyup', this.signInClickById);
        }
    }

    signInClickById = (event) => {
        event.preventDefault()
        if (event.keyCode === 13) {
            $('#sign_in').click()
        }
    }

    addEventListenerOnKeyDown = () => {
        document.getElementById("password").addEventListener("keyup", (event) => this.signInClickById(event));
        document.getElementById("email").addEventListener("keyup", (event) => this.signInClickById(event));
    }

    fetchUserAndProData = (authToken, isRequestFreeProposal) => {
        const {history, location} = this.props
        const {search} = location
        axios.create({
            baseURL: apiBaseUrl,
            headers: {
                'Authorization': `Bearer ${authToken}`,
                'x-wiz-device-type': 'WEB'
            }
        }).get('rest-auth/user/').then(resp => {
                const data = apiv1.parseAndAutoHandleIssues(resp, this.props.doDispatch);
                if (data) {
                    localStorage.firstName = data.first_name
                    localStorage.lastName = data.last_name
                    if (data.client.is_default_professional) {
                        this.setState({ loading: false })
                        $('#create_account_of_user_type').click()
                        localStorage.clear()
                    } else {
                        if (isRequestFreeProposal === path.forConsultation){
                            history.replace(`${path.findAProfessionalByLocation}/true`)
                        } else {
                            history.replace({
                                pathname: path.userOnboarding,
                                search,
                            })
                            // history.replace(path.userOnboarding)
                        }
                    }
                } else {
                    this.setState({ loading: false })
                }
            })
            .catch(err => {
                this.setState({ loading: false });
                apiv1.handleErrors(err, this.props.doDispatch)
            })
    };

    onSignInClick = (e) => {
        e.preventDefault();
        const { email, password } = this.state;
        let request = {
            email,
            password
        };
        const errors = {};
        const isFormValidated = true;
        if (isFormValidated === true) {
            this.setState({ loading: true, error: {} });
            apiv1.noAuth.post('rest-auth/login/', request)
                .then((resp) => {
                    const data = apiv1.parseAndAutoHandleIssues(resp);
                    if (data) {
                        // this.props.onLogin(data.key);
                        this.setState({loading:false})
                        const {isRequestFreeProposal} = this.props.match.params
                        if (isRequestFreeProposal !== undefined && isRequestFreeProposal){
                            localStorage.authToken = 'Bearer ' + data.key
                            this.fetchUserAndProData(data.key, isRequestFreeProposal)
                        } else {
                            this.props.onLogin(data.key)
                        }
                        // if (this.props.type === 'user') {
                        //     this.props.history.push({
                        //         pathname: path.userDashboard,
                        //     })
                        // } else {
                        //     this.props.history.push({
                        //         pathname: path.professionalDashboard
                        //     })
                        // }
                    } else {
                        this.setState({ loading: false })
                    }
                })
                .catch((err) => {
                    const resErrors = apiv1.handleErrors(err, this.props.doDispatch);
                    for (let key in resErrors) {
                        errors[key] = resErrors[key][0]
                    }
                    this.setState({ loading: false, errors, email: '', password: '' })
                })
        } else {
            this.setState({ loading: false, errors })
        }

    };

    signWithWithGoogle = (googleUser) => {
        const errors = {};
        const request = {
            access_token: googleUser.accessToken
        };
        this.setState({ loading: true, error: {} });
        apiv1.noAuth.post('/rest-auth/google/connect/', request)
            .then((resp) => {
                this.setState({ loading: false });
                const data = apiv1.parseAndAutoHandleIssues(resp);
                if (data) {
                    this.props.onLogin(data.key);
                    // askForPermissioToReceiveNotifications()
                } else {
                    this.setState({ loading: false })
                }
            })
            .catch((err) => {
                const resErrors = apiv1.handleErrors(err, this.props.doDispatch);
                for (let key in resErrors) {
                    errors[key] = resErrors[key][0]
                }
                this.setState({ loading: false, errors, email: '', password1: '' })
            })
    };

    onChange = (e) => {
        const newErrors = { ...this.state.errors };
        newErrors[e.target.name] = '';
        this.setState({ [e.target.name]: e.target.value, errors: newErrors })
    };

    render() {
        const { email, password, loading, errors, authToken } = this.state;

        return (
            <div>
                <MetaTags>
                    <title>Log in</title>
                    <meta name="title" content="Log in"/>
                    <meta name="description" content="Online Legal Services, Free Legal advice, On Demand Legal Consultation, Accounting Services, Online Tax Consultation"/>
                    <meta name="keywords" content="Lawyer for Startup, Online Legal Advice, Online legal Advice free, Online Legal Services, Hire a lawyer online, Lawyers near me, Online Consultation with Lawyer, Online Lawyer Advice, Online Lawyer Service, Ask a Lawyer, Tax Lawyers near me, Tax Attorney near me, Civil Lawyers near me, Hire accountant online, CA near me, Chartered Accountant near me, Tax consultation near me, Income tax consultation online, Bookkeeping services near me, bookkeeping services online, Company Registration online, LLP registration Online, "/>
                    <meta name="url" content="https://wizcounsel.com/login"/>
                    <meta property="og:url" content="https://wizcounsel.com/login"/>
                    <meta property="og:type"   content="website" />
                    <meta property="og:site_name" content="WizCounsel"/>
                    <meta property="og:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                    <meta property="og:image:width" content="300"/>
                    <meta property="og:image:height" content="300"/>
                    <meta name="twitter:card" content="Online Legal Services, Free Legal advice, On Demand Legal Consultation, Accounting Services, Online Tax Consultation"/>
                    <meta name="twitter:site" content="WizCounsel" />
                    <meta name="twitter:title" content="Log in" />
                    <meta name="twitter:description" content="Online Legal Services, Free Legal advice, On Demand Legal Consultation, Accounting Services, Online Tax Consultation" />
                    <meta name="twitter:image" content="https://s3.amazonaws.com/wiz-public-cdn/images/logowhatsapp.png"/>
                    <link rel="canonical" href="https://wizcounsel.com/login"/>
                </MetaTags>
                {loading ?
                    (<div style={{ marginTop: '25%', marginLeft: '50%' }}> <LoadingComponent pastDelay /> </div>)
                    :
                    (<section>
                        <div className="container" style={{marginTop:'3%'}}>
                            <div className="row">
                                <div className="col-sm-6 offset-md-3">
                                    <div className="col-sm-12">
                                        {/*<span className={(errors.non_field_errors) ? 'error' : 'no-error'} >{errors.non_field_errors}</span>*/}
                                        <h1 className="ui-sign-heading">Sign in</h1>

                                        <div className={(errors.non_field_errors) ? 'msg msg-danger msg-danger-text' : 'hidden'}>
                                        <span className="glyphicon glyphicon-remove pull-right"
                                              style={{ cursor: 'pointer' }}
                                              onClick={() => this.setState({ errors: {} })}
                                        ></span>
                                            {errors.non_field_errors}
                                        </div>
                                        {this.props.location.message !== undefined &&
                                        (
                                            <div className="alert alert-success alert-dismissible">
                                                <a href="#" className="close" data-dismiss="alert"
                                                   aria-label="close">&times;</a>
                                                {this.props.location.message}
                                            </div>
                                        )
                                        }
                                    </div>
                                    <div className="clearfix"></div>
                                    <form className="signup-form">
                                        <div className="col-sm-12">
                                            {/*<GoogleLogin*/}
                                            {/*clientId="995918006792-f1n8kout8sod6p8p1m99vl1cgbt0k34j.apps.googleusercontent.com"*/}
                                            {/*render={renderProps => (*/}
                                            {/*<button onClick={renderProps.onClick} className="btn btn-block btn-google-plus">Connect with Google</button>*/}
                                            {/*)}*/}
                                            {/*buttonText="Login"*/}
                                            {/*onSuccess={this.signWithWithGoogle}*/}
                                            {/*onFailure={this.onFailure}*/}
                                            {/*/>*/}
                                            {/*<h4 className="text-dark text-center">or</h4>*/}
                                            {/*<button className="btn btn-block btn-google-plus">Connect with Google</button>*/}
                                            {/*<h4 className="text-dark text-center">or</h4>*/}
                                            <div className="form-group">
                                                <input type="text"
                                                       className="form-control input-lg"
                                                       value={email}
                                                       onChange={this.onChange}
                                                       name="email"
                                                       style={{ borderBottom: errors.email ? '1px solid #ff3366' : '' }}
                                                       placeholder="Email"
                                                       id='email'
                                                       autoFocus
                                                />
                                                <span className={(errors.email) ? 'error' : 'no-error'} >{errors.email}</span>
                                            </div>
                                        </div>
                                        <div className="col-sm-12">
                                            <div className="form-group">
                                                <input type="password"
                                                       className="form-control input-lg"
                                                       value={password}
                                                       onChange={this.onChange}
                                                       name="password"
                                                       id='password'
                                                       style={{ borderBottom: errors.password ? '1px solid #ff3366' : '' }}
                                                       placeholder="Password"
                                                />
                                                {/*<span style={{position:'absolute',right:'15px',top:'25px',fontSize:'18px'}} id="password_visibility" className="fa fa-eye"></span>*/}
                                                <span className={errors.password ? 'error' : 'no-error'}>{errors.password}</span>
                                                <a className="pull-right text-dark"
                                                   onClick={() => {
                                                       this.props.history.push(path.resetPasswordLink)
                                                   }}
                                                >Forgot Password ?</a>
                                            </div>

                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="col-sm-12 text-center">
                                            <p>&#x00A0;</p>
                                            <a>
                                                {!localStorage.isSessionValid() ?
                                                <button type="button"
                                                        id='sign_in'
                                                        className="btn btn-submit btn-lg"
                                                        onClick={(e) => this.onSignInClick(e, false)}
                                                >SIGN IN</button>
                                                 :
                                                    <button type="button"
                                                            id='sign_in'
                                                            className="btn btn-submit btn-lg"
                                                            onClick={(e) => {
                                                            this.setState({"aaa" : 'dsds'})
                                                                localStorage.clear()
                                                            }
                                                            }
                                                    >SIGN OUT</button>
                                                }
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </section>)
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
};

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (authToken) => {
            dispatch(sessionLogin({
                auth_token: authToken,
            }))
        },
        ...(errorActionDispatchers(dispatch))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UserAndProLogIn))

export const create_user_accout_message = 'You\'re already registered as a Professional with us. Please use different email id to check out as a client.'
