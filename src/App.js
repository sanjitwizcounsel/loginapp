import React, {Component} from 'react';
import { Route, Switch, BrowserRouter as Router} from 'react-router-dom'
import UserAndProLogIn from "./components/common/UserAndProLogIn";
import * as path from './constants/path'
import { applyMiddleware, createStore, compose } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import reducer from './reducers';
import logger from 'redux-logger'
const middleware = [thunk, logger];

let store = '';

store = createStore(
    reducer,
    applyMiddleware(...middleware),
);

class App extends Component{
  render(){
      return(
          <Provider store={store}>
          <div className="App">
              <Router>
                   <Route path={path.root} component={UserAndProLogIn} />
              </Router>
          </div>
          </Provider>
      )
  }
}

export default App;
